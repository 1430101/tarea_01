Cadena str;
Boton b1,b2,b3,b4,b5,b6,b7;
PFont p;
String men;
String men2;
public void setup(){
    size(400,200);
    smooth();
    str = new Cadena("RODOLFO");
    p = createFont("Arial",14);
    textFont(p);
    b1 = new Boton(20,25,"INVIERTE");
    b2 = new Boton(100,25,"BORRAR INICIO");
    b3 = new Boton(180,25,"VACIO");
    b4 = new Boton(20,100,"AGREGAR FINAL");
    b5 = new Boton(100,100,"AGREGAR INICIO");
    b6 = new Boton(180,100,"LLENA");
    b7 = new Boton(300,65,"BORRAR FINAL");
    men2 ="Aqui va el mensaje";
}

public void draw(){
     background(127);
     fill(0,255,0);
     text(str.toString(),20,height/2);
     b1.dibujar();
     b2.dibujar();
     b3.dibujar();
     b4.dibujar();
     b5.dibujar();
     b6.dibujar();
     b7.dibujar();
     
     fill(255);
     text(men2,20,height-30);
}

public void mousePressed(){
    men = b1.click(mouseX, mouseY);
    if(men.equals("INVIERTE")){
       str.cadena = str.invertir(str.cadena);
       men2="INVIERTE";
    }else
       men2="Esta afuera";
    men = b2.click(mouseX, mouseY);
    if(men.equals("BORRAR INICIO")){
       str.cadena = str.invertir(str.cadena);
       men2="BORRAR INICIO";
    }
    men = b3.click(mouseX, mouseY);
    if(men.equals("VACIO")){
       str.cadena = str.invertir(str.cadena);
       men2="VACIO";
    }
    men = b4.click(mouseX, mouseY);
    if(men.equals("AGREGAR FINAL")){
       str.cadena = str.invertir(str.cadena);
       men2="AGREGAR FINAL";
    }
    men = b5.click(mouseX, mouseY);
    if(men.equals("AGREGAR INICIO")){
       str.cadena = str.invertir(str.cadena);
       men2="AGREGAR INICIO";
    }
    men = b6.click(mouseX, mouseY);
    if(men.equals("LLENA")){
       str.cadena = str.invertir(str.cadena);
       men2="LLENA";
    }
    men = b7.click(mouseX, mouseY);
    if(men.equals("BORRAR FINAL")){
       str.cadena = str.invertir(str.cadena);
       men2="BORRAR FINAL";
    }
    
}